<?php

declare(strict_types=1);


use Bittacora\Bpanel4\Payment\DomiciledReceipt\Http\Controllers\DomiciledReceiptPaymentAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/pago-por-recibo-domiciliado')->name('bpanel4-domiciled-receipt.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/configurar', [DomiciledReceiptPaymentAdminController::class, 'index'])->name('index');
        Route::put('/guardar-configuracion', [DomiciledReceiptPaymentAdminController::class, 'update'])->name('update');
    });
