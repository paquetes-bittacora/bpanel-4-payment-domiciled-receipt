<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Tests\Acceptance;

use Bittacora\Bpanel4\Payment\DomiciledReceipt\Models\DomiciledReceiptConfig;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class DomiciledReceiptAdminRoutesTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        DomiciledReceiptConfig::create();
        $this->actingAs((new UserFactory())->withPermissions('bpanel4-domiciled-receipt.index', 'bpanel4-domiciled-receipt.update')
            ->createOne());
    }

    public function testMuestraLaVistaDeEdicionDeReciboDomiciliado(): void
    {
        $response = $this->get(route('bpanel4-domiciled-receipt.index'));
        $response->assertOk();
        $response->assertSee('Pago por recibo domiciliado');
    }

    public function testSePuedeGuardarLaConfiguracionDelPagoPorReciboDomiciliado(): void
    {
        $instructions = $this->faker->text();
        $response = $this->followingRedirects()->put(route('bpanel4-domiciled-receipt.update'), ['instructions' => $instructions]);
        $response->assertOk();
        $response->assertSee('Configuración actualizada');
    }
}
