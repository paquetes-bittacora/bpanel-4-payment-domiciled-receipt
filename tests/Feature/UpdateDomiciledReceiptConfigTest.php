<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Tests\Feature;

use Bittacora\Bpanel4\Payment\DomiciledReceipt\Actions\UpdateDomiciledReceiptConfiguration;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Dtos\DomiciledReceiptConfigDto;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Models\DomiciledReceiptConfig;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class UpdateDomiciledReceiptConfigTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        DomiciledReceiptConfig::create();
    }

    public function testActualizaLaConfiguracionDelPagoPorReciboDomiciliado(): void
    {
        $action = $this->app->make(UpdateDomiciledReceiptConfiguration::class);

        $instructions = 'instrucciones del pago por transferencia ' . time();
        $action->handle(new DomiciledReceiptConfigDto($instructions));

        self::assertEquals($instructions, DomiciledReceiptConfig::first()->user_instructions);
    }
}
