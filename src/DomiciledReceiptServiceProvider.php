<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt;

use Bittacora\Bpanel4\Payment\DomiciledReceipt\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

final class DomiciledReceiptServiceProvider extends ServiceProvider
{
    public const PACKAGE_PREFIX = 'bpanel4-domiciled-receipt';

    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang/', self::PACKAGE_PREFIX);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->commands(InstallCommand::class);
    }
}
