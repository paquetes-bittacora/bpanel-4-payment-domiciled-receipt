<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\PaymentMethods;

use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Repositories\OrderRepository;
use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Models\DomiciledReceiptConfig;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

final class DomiciledReceipt implements PaymentMethod
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly Redirector $redirector,
    ) {
    }

    public function processPayment(OrderDetailsDto $orderDetails): RedirectResponse
    {
        $order = $this->orderRepository->getById($orderDetails->orderId);
        $order->setStatus(OrderStatusModel::whereId(OrderStatus::PAYMENT_PENDING->value)->firstOrFail());
        return $this->redirector->route('order.confirmed');
    }

    public function getUserInstructions(): ?string
    {
        return DomiciledReceiptConfig::firstOrFail()->user_instructions;
    }

    public function getName(): string
    {
        return 'Pago por recibo domiciliado';
    }
}
