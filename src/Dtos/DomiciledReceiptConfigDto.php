<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Dtos;

final class DomiciledReceiptConfigDto
{
    public function __construct(public readonly string $instructions = '')
    {
    }
}
