<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Models\DomiciledReceiptConfig;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\PaymentMethods\DomiciledReceipt;
use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Payment;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    private const PERMISSIONS = ['index', 'update'];

    /**
     * @var string
     */
    protected $signature = 'bpanel4-domiciled-receipt:install';
    /**
     * @var string
     */
    protected $description = 'Registra las opciones de pago por recibo domiciliado en el menú.';

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    public function handle(AdminMenu $adminMenu): void
    {
        $this->addMenuEntries($adminMenu);
        $this->giveAdminPermissions();
        $this->createDefaultConfiguration();
        $this->registerPaymentMethod();
    }

    private function addMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createAction(
            'bpanel4-payment',
            'Pago por recibo domiciliado',
            'index',
            'far fa-receipt',
            'bpanel4-domiciled-receipt.index',
        );
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-domiciled-receipt.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createDefaultConfiguration(): void
    {
        DomiciledReceiptConfig::create();
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    private function registerPaymentMethod(): void
    {
        /** @var Payment $paymentModule */
        $paymentModule = resolve(Payment::class);
        /** @var DomiciledReceipt $paymentMethod */
        $paymentMethod = resolve(DomiciledReceipt::class);
        $paymentModule->registerPaymentMethod($paymentMethod);
    }
}
