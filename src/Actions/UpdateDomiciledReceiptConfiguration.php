<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Actions;

use Bittacora\Bpanel4\Payment\DomiciledReceipt\Dtos\DomiciledReceiptConfigDto;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Models\DomiciledReceiptConfig;

final class UpdateDomiciledReceiptConfiguration
{
    public function handle(DomiciledReceiptConfigDto $dto): void
    {
        $config = DomiciledReceiptConfig::firstOrFail();
        $config->user_instructions = $dto->instructions;
        $config->save();
    }
}
