<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $user_instructions
 * @method static self firstOrFail()
 */
final class DomiciledReceiptConfig extends Model
{
    /**
     * @var string
     */
    protected $table = 'payment_methods_domiciled_receipt_config';
}
