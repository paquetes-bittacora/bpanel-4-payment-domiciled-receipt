<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Actions\UpdateDomiciledReceiptConfiguration;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Dtos\DomiciledReceiptConfigDto;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Http\Requests\UpdateDomiciledReceiptConfigRequest;
use Bittacora\Bpanel4\Payment\DomiciledReceipt\Models\DomiciledReceiptConfig;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;

final class DomiciledReceiptPaymentAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $router,
        private readonly Redirector $redirector,
    ) {
    }

    public function index(): View
    {
        $this->authorize('bpanel4-domiciled-receipt.index');
        return $this->view->make('bpanel4-domiciled-receipt::index', [
            'action' => $this->router->route('bpanel4-domiciled-receipt.update'),
            'paymentMethod' => DomiciledReceiptConfig::firstOrFail(),
        ]);
    }

    public function update(
        UpdateDomiciledReceiptConfigRequest $request,
        UpdateDomiciledReceiptConfiguration $updateBankWireConfiguration
    ): RedirectResponse {
        $this->authorize('bpanel4-domiciled-receipt.update');
        try {
            $updateBankWireConfiguration->handle(new DomiciledReceiptConfigDto($request->validated('instructions')));
            return $this->redirector->route('bpanel4-domiciled-receipt.index')
                ->with(['alert-success' => 'Configuración actualizada']);
        } catch (Exception) {
            return $this->redirector->route('bpanel4-domiciled-receipt.index')
                ->with(['alert-danger' => 'Ocurrió un error al actualizar la configuración']);
        }
    }
}
