<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\DomiciledReceipt\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class UpdateDomiciledReceiptConfigRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'instructions' => 'nullable|string',
        ];
    }
}
