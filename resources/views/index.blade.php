@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-domiciled-receipt::general.index'))

@section('content')
    @include('bpanel4-domiciled-receipt::_form')
@endsection
