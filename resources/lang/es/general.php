<?php

declare(strict_types=1);

return [
    'index' => 'Opciones de pago por recibo domiciliado',
    'instructions' => 'Instrucciones para los clientes',
];
