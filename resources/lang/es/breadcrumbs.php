<?php

declare(strict_types=1);

return [
    'bpanel4-domiciled-receipt' => 'Pago por recibo domiciliado',
    'index' => 'Opciones',
];
